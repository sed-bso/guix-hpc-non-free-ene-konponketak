;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2024 Inria

(define-module (guix-hpc-non-free packages cpp)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages cpp)
  #:use-module (guix-science-nonfree packages cuda))

;; This creates a kokkos version with specific tweaks for gyselalibxx
(define (make-kokkos-cuda name cuda-arch cuda-package)
  (package/inherit kokkos
    (name name)
    (arguments (substitute-keyword-arguments (package-arguments kokkos)
                 ((#:configure-flags flags)
                  #~(append (list "-DKokkos_ENABLE_CUDA=ON"
                                  ;; Needed for Gyselalibxx
                                  "-DKokkos_ENABLE_CUDA_RELOCATABLE_DEVICE_CODE=ON"
                                  "-DKokkos_ENABLE_CUDA_CONSTEXPR=ON"
                                  ;; Target architecture must be set since the build
                                  ;; machine doesn't have the hardware.
                                  ;; This is the oldest supported architecture by
                                  ;; kokkos for compatibility reasons
                                  (string-append "-DKokkos_ARCH_" #$cuda-arch "=ON"))
                            ;; Relocatable code is not compatible with shared libs
                            (delete "-DBUILD_SHARED_LIBS=ON"
                                    #$flags)))
                 ;; Cannot run tests due to lack of specific hardware
                 ((#:tests? _ #t)
                  #f)
                 ;; RUNPATH validation fails since libcuda.so.1 is not present at build
                 ;; time.
                 ((#:validate-runpath? #f #f)
                  #f)
                 ((#:phases phases
                   '%standard-phases)
                  #~(modify-phases #$phases
                      ;; File is not present in CUDA build
                      (delete 'remove-cruft)))))
    (inputs (modify-inputs (package-inputs kokkos)
              ;; Older architectures are not supported by CUDA 12
              (prepend cuda-package)))))

(define-public kokkos-cuda-k40
  ;; This architecture is not supported by CUDA 12
  (make-kokkos-cuda "kokkos-cuda-k40" "KEPLER35" cuda-11))

(define-public kokkos-cuda-a40
  (make-kokkos-cuda "kokkos-cuda-a40" "AMPERE86" cuda))

(define-public kokkos-cuda-a100
  (make-kokkos-cuda "kokkos-cuda-a100" "AMPERE80" cuda))

(define-public kokkos-cuda-v100
  (make-kokkos-cuda "kokkos-cuda-v100" "VOLTA70" cuda))

(define-public kokkos-cuda-p100
  (make-kokkos-cuda "kokkos-cuda-p100" "PASCAL60" cuda))
