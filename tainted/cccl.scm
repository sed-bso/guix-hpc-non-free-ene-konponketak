;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Note that this module provides packages that depend on "non-free"
;;; software, which denies users the ability to study and modify it.
;;;
;;; Copyright © 2019, 2022, 2023 Inria

(define-module (tainted cccl)
  #:use-module (guix)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages)
  #:use-module (guix-science-nonfree packages cuda))

(define-public cccl
  (package
    (name "cccl")
    (version "2.3.1")
    (home-page "https://github.com/NVIDIA/cccl")
    (synopsis "CUDA C++ Core Libraries (CCCL)")
    (description "This CUDA C++ Core Libraries (CCCL) unifies three essential 
        CUDA C++ libraries into a single, convenient repository: Thrust, CUB and 
        libcudacxx. The goal of CCCL is to provide CUDA C++ developers with building 
        blocks that make it easier to write safe and efficient code. Bringing these 
        libraries together streamlines your development process and broadens your 
        ability to leverage the power of CUDA C++.")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url home-page)
              (recursive? #t)
              (commit "54fd4c00a8a796d4d4497ccd82651768539d5e30")))
        (file-name (string-append name "-" version "-checkout"))
        (sha256 (base32 "0v4g03gxbj2j81w6mln1d4xqmqifi0g9vcs8zyv95cjfgwkb95a4"))))
    (inputs
     (list cuda))
    (build-system cmake-build-system)
    (arguments (list #:configure-flags #~(list "-DCCCL_ENABLE_CUB=OFF"
					"-DCCCL_ENABLE_TESTING=OFF"
					"-DCCCL_ENABLE_THRUST=OFF"
					"-DLIBCUDACXX_ENABLE_LIBCXXABI_TESTS=OFF"
					"-DLIBCUDACXX_ENABLE_LIBCUDACXX_TESTS=OFF"
					"-DLIBCUDACXX_ENABLE_LIBCUDACXX_TESTS=OFF"
					"-DLIBCUDACXX_ENABLE_LIBUNWIND_TESTS=OFF")
                     #:tests? #f
                     #:build-type "Release"))
    (license license:asl2.0)))

