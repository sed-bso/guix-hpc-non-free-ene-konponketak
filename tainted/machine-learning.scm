;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; However, note that this module provides packages for "non-free" software,
;;; which denies users the ability to study and modify it.  These packages
;;; are detrimental to user freedom and to proper scientific review and
;;; experimentation.  As such, we kindly invite you not to share it.
;;;
;;; Copyright © 2023 Inria

(define-module (tainted machine-learning)
  ;; This module is kept for backward-compatibility.
  ;; Use (guix-science-nonfree packages machine-learning) instead.
  #:use-module ((guix packages) #:select (deprecated-package))
  #:use-module (guix-science-nonfree packages machine-learning))

(define-public python-pytorch-cuda
  (deprecated-package "python-pytorch-cuda"
                      python-pytorch-with-cuda11))
